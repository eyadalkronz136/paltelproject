package com.eyad.alkronz.paltelproject.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;

import com.eyad.alkronz.paltelproject.Fragment.EventsFragment;
import com.eyad.alkronz.paltelproject.Fragment.HomeFragment;
import com.eyad.alkronz.paltelproject.Fragment.NotificationFragment;
import com.eyad.alkronz.paltelproject.Fragment.ProfileFragment;
import com.eyad.alkronz.paltelproject.Fragment.SettingsFragment;
import com.eyad.alkronz.paltelproject.HomeActivity;
import com.eyad.alkronz.paltelproject.R;
import com.eyad.alkronz.paltelproject.Utility.FragmentsUtil;

/**
 * Created by Eyad on 9/1/2017.
 */

public class MyOnNavigationItemSelectedListener implements BottomNavigationView.OnNavigationItemSelectedListener {

    Fragment HomeFragment = new HomeFragment();
    Fragment ProfileFragment = new ProfileFragment();
    Fragment NotificationFragment = new NotificationFragment();
    Fragment EventsFragment = new EventsFragment();
    Fragment SettingsFragment = new SettingsFragment();

    FragmentActivity mContext;
   public MyOnNavigationItemSelectedListener(FragmentActivity mContext){
       this.mContext=mContext;
   }
     @Override


        public boolean onNavigationItemSelected( MenuItem item) {

        switch (item.getItemId()) {

                case R.id.navigation_home:
                    FragmentsUtil.replaceFragment(mContext ,R.id.content ,HomeFragment);
                    return true;

                case R.id.navigation_profile:
                     FragmentsUtil.replaceFragment(mContext ,R.id.content ,ProfileFragment);
                    return true;


                case R.id.navigation_notifications:
                     FragmentsUtil.replaceFragment(mContext ,R.id.content ,NotificationFragment);
                    return true;

                case R.id.navigation_events:
                     FragmentsUtil.replaceFragment(mContext ,R.id.content ,EventsFragment);
                    return true;


                case R.id.navigation_dashboard:
                     FragmentsUtil.replaceFragment(mContext ,R.id.content ,SettingsFragment);
                    return true;
            }
            return false;
        }
}

/*
case 0 : return  new HomeFragment();
            case 1 : return  new ProfileFragment();
            case 2 : return  new NotificationFragment();
            case 3 : return  new EventsFragment();
            case 4 : return  new SettingsFragment();
 */
