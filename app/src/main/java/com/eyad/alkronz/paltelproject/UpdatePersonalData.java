package com.eyad.alkronz.paltelproject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class UpdatePersonalData extends AppCompatActivity {

    private  static  final int gallery = 200;
    ImageButton btnChangeImage;

    TextView textstatus_display , display_nametv;
    CircleImageView  setting_image;

    DatabaseReference databaseReference ;
    FirebaseUser currentUser ;

    StorageReference mImageStorage;

    TextView skipbtn;

    ProgressDialog mProgressDialog ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        skipbtn = (TextView) findViewById(R.id.skipbtn);
        skipbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UpdatePersonalData.this , HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_CLEAR_TASK);
                 startActivity(intent);
            }
        });

        btnChangeImage = (ImageButton) findViewById(R.id.btnChangeImage);
        btnChangeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToChangeImage();
            }
        });

        textstatus_display  = (TextView) findViewById(R.id.textstatus_display);
        display_nametv  = (TextView) findViewById(R.id.display_nametv);
        setting_image  = (CircleImageView) findViewById(R.id.setting_image);

        mProgressDialog = new ProgressDialog(this);

        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String uid = currentUser.getUid();


        databaseReference = FirebaseDatabase.getInstance().getReference().child("users").child(uid);
        databaseReference.keepSynced(true);

        mImageStorage = FirebaseStorage.getInstance().getReference();




        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String name =  dataSnapshot.child("FullName").getValue(String.class);
                String status =  dataSnapshot.child("BIO").getValue(String.class);
                String thumb_image =  dataSnapshot.child("ThumbImage").getValue(String.class);
                final String image   =  dataSnapshot.child("UserImage").getValue(String.class);

                display_nametv.setText(name);
                textstatus_display.setText(status);

                if(!image.equals("default"))
                //  Picasso.with(SettingActivity.this).load(image).placeholder(R.drawable.default_profle_image).into(setting_image);
                Picasso.with(UpdatePersonalData.this).load(image).networkPolicy(NetworkPolicy.OFFLINE)
                        .placeholder(R.drawable.default_profle_image_male)
                        .into(setting_image, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                Picasso.with(UpdatePersonalData.this).load(image).placeholder(R.drawable.default_profle_image_male).into(setting_image);
                            }
                        });


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



    public void goToChangeImage() {
          CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON).setAspectRatio(1,1).setMinCropWindowSize(500,500) //to be square
                .start(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data){
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mProgressDialog .setTitle("uplod your Image");
                mProgressDialog.setMessage("please wait while uploding your profile image");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();

                Uri resultUri = result.getUri();
                File thumpFilePath = new File(resultUri.getPath());

                Bitmap thum_bitmap = null;
                try {
                      thum_bitmap  = new Compressor(this).
                              setMaxHeight(200)
                              .setMaxWidth(200)
                              .setQuality(75)
                              .compressToBitmap(thumpFilePath);


                } catch (IOException e) {
                    e.printStackTrace();
                }



                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                thum_bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                final byte[] dataBytes = baos.toByteArray();

                StorageReference filePath = mImageStorage.child("profile_images").child(currentUser.getUid()+".jpg");
                final StorageReference thump_filePath = mImageStorage.child("profile_images").child("thump_file").child(currentUser.getUid()+".jpg");

                final Map updateMap = new HashMap<>();
                filePath.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull final Task<UploadTask.TaskSnapshot> task) {
                        if(task.isSuccessful())
                        {
                            Toast.makeText(UpdatePersonalData.this, "Done uplod your profile image ", Toast.LENGTH_SHORT).show();
                            String downlodURL = task.getResult().getDownloadUrl().toString();
                            updateMap.put("UserImage",downlodURL);
                            //databaseReference.child("image").setValue(downlodURL);

                            UploadTask uploadTask = thump_filePath.putBytes(dataBytes);
                            uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                String thump_downlod_url ;
                                @Override
                                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> thump_task) {
                                    if(thump_task.isSuccessful()){
                                        thump_downlod_url=thump_task.getResult().getDownloadUrl().toString();
                                        updateMap.put("ThumbImage",thump_downlod_url);
                                        databaseReference.updateChildren(updateMap);
                                    }
                                }});

                            mProgressDialog.dismiss();

                        }
                        else
                            Toast.makeText(UpdatePersonalData.this, "fail uplod your profile image !!! ", Toast.LENGTH_SHORT).show();
                    }
                });

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }


    public void GoToChangeBio(View view) {
        Intent intent = new Intent(this , UpdateBio.class );
        startActivity(intent);
    }
}
