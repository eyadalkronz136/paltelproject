package com.eyad.alkronz.paltelproject.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eyad.alkronz.paltelproject.Entity.MyNotification;
import com.eyad.alkronz.paltelproject.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment {


    String u_id = FirebaseAuth.getInstance().getCurrentUser().getUid();


    DatabaseReference NotificationDatabaseReference;
    static DatabaseReference UsersDatabaseReference;
    RecyclerView listOfNotification;

    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_notification, container, false);
        NotificationDatabaseReference = FirebaseDatabase.getInstance().getReference().child("Notifications").child(u_id);
        UsersDatabaseReference = FirebaseDatabase.getInstance().getReference().child("users");
        listOfNotification = (RecyclerView) mView.findViewById(R.id.listOfNotification);
        listOfNotification.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        return mView;
    }


    @Override
    public void onStart() {
        super.onStart();


        FirebaseRecyclerAdapter<MyNotification, NotificationViewHolder> firebaseRecyclerAdapter =
                new FirebaseRecyclerAdapter<MyNotification, NotificationViewHolder>(
                        MyNotification.class,
                        R.layout.notification_row,
                        NotificationViewHolder.class,
                        NotificationDatabaseReference
                ) {
                    @Override
                    protected void populateViewHolder(final NotificationViewHolder viewHolder, final MyNotification model, int position) {
                        viewHolder.setDate(model);
                    }
                };
        listOfNotification.setAdapter(firebaseRecyclerAdapter);
    }


    public static class NotificationViewHolder extends RecyclerView.ViewHolder {
        View mView;
        CircleImageView notification_image;
        TextView notification_txt, notification_from;

        public NotificationViewHolder(View itemView) {
            super(itemView);
            mView = itemView;

            notification_image = (CircleImageView) itemView.findViewById(R.id.notification_image);
            notification_txt = (TextView) itemView.findViewById(R.id.notification_txt);
            notification_from = (TextView) itemView.findViewById(R.id.notification_from);
        }

        public void setDate(final MyNotification model) {
            UsersDatabaseReference.child(model.userId).child("FullName").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() ==null)return;
                    notification_from.setText(dataSnapshot.getValue().toString());
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });


            UsersDatabaseReference.child(model.userId).child("ThumbImage").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(final DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() ==null)return;


                    String path =  dataSnapshot.getValue().toString();
                    if(path != null && path.length()>1)
                    Picasso.with(mView.getContext()).load(path).networkPolicy(NetworkPolicy.OFFLINE).
                            into(notification_image, new Callback() {
                                @Override
                                public void onSuccess() {
                                }

                                @Override
                                public void onError() {
                                    Picasso.with(mView.getContext()).load(dataSnapshot.getValue().toString()).into(notification_image);
                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });


            notification_txt.setText(model.message);
        }


    }

}
