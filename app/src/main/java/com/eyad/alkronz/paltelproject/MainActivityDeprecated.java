package com.eyad.alkronz.paltelproject;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.RemoteInput;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.eyad.alkronz.paltelproject.Adapters.SectionsPagerAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class MainActivityDeprecated extends AppCompatActivity {

    static final int REQUEST_VIDEO_CAPTURE = 1;

    StorageReference VideoReference;


    private FirebaseAuth mAuth;
    private Toolbar toolbar ;

    DatabaseReference databaseReference;

    private TabLayout tableLayout;

    private ViewPager viewPager ;
    private SectionsPagerAdapter sectionsPagerAdapter;



    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        VideoReference = FirebaseStorage.getInstance().getReference();


        if(FirebaseAuth.getInstance().getCurrentUser() == null){
            Intent intent = new Intent(this, StartActivity.class);
            startActivity(intent);
            finish();
        }



        viewPager = (ViewPager) findViewById(R.id.main_tabpager);
        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(sectionsPagerAdapter);

        tableLayout = (TabLayout) findViewById(R.id.tab_layout);
        tableLayout.setupWithViewPager(viewPager);

        tableLayout.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {

                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                         int tabIconColor = ContextCompat.getColor(MainActivityDeprecated.this , R.color.colorAccent);
                        tab.getIcon().setTint(tabIconColor);
                    }

                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                        int tabIconColor = ContextCompat.getColor(MainActivityDeprecated.this , R.color.colorPrimary);
                        tab.getIcon().setTint(tabIconColor);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }
                }
        );

        mAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("users");


        tableLayout.getTabAt(0).setIcon(R.drawable.ic_home);
        int tabIconColor = ContextCompat.getColor(MainActivityDeprecated.this , R.color.colorAccent);
        tableLayout.getTabAt(0). getIcon().setTint(tabIconColor);

        tableLayout.getTabAt(1).setIcon(R.drawable.ic_user_profile);
        tableLayout.getTabAt(2).setIcon(R.drawable.ic_notifications_icon);
        tableLayout.getTabAt(3).setIcon(R.drawable.ic_events);
        tableLayout.getTabAt(4).setIcon(R.drawable.ic_settings);
       }




    @Override
    protected void onStop() {
        super.onStop();
        // databaseReference.child("online").setValue("false");

    }

    private void sendToStart() {
        Intent intent = new Intent(this , StartActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
            Uri videoUri = data.getData();
           // mVideoView.setVideoURI(videoUri);
           //code to upload video to fireBase







            Uri file = videoUri;
             StorageReference riversRef = VideoReference.child("Videos/"+mAuth.getCurrentUser().getUid()+".mp4");
            UploadTask uploadTask = riversRef.putFile(file);
            Toast.makeText(this, "Upload video In Background  ....", Toast.LENGTH_SHORT).show();
// Register observers to listen for when the download is done or if it fails
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    Toast.makeText(MainActivityDeprecated.this, "Done upload Video", Toast.LENGTH_SHORT).show();


                    databaseReference .child(mAuth.getCurrentUser().getUid()).child("VideoURL").setValue(downloadUrl.toString());

                }
            });




    }






}
