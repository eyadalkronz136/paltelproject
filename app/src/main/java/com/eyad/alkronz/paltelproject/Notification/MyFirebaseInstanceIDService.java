package com.eyad.alkronz.paltelproject.Notification;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Eyad on 8/13/2017.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {


     private FirebaseAuth mAuth;
     private DatabaseReference mDatabaseReference;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();


        String u_id = FirebaseAuth.getInstance().getCurrentUser().getUid();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference().child("users");
        String new_token = FirebaseInstanceId.getInstance().getToken();

        sendTokenToServer(u_id ,new_token );
    }

    private void sendTokenToServer(String u_id, String new_token) {
        mDatabaseReference.child(u_id).child("device_token").setValue(new_token);
    }
}
