package com.eyad.alkronz.paltelproject;

import android.app.DatePickerDialog;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class UpdateExtraData extends AppCompatActivity {
    DatabaseReference databaseReference ;
    TextInputEditText  city_tx , address_tx , email_tx , mobile_tx  , phone_tx  ;
    String u_id = FirebaseAuth.getInstance().getCurrentUser().getUid();

    RadioButton male , female ;
    TextInputEditText DOBText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_extra_data);



        city_tx = (TextInputEditText) findViewById(R.id.city_tx);
        address_tx = (TextInputEditText) findViewById(R.id.address_tx);
        email_tx = (TextInputEditText) findViewById(R.id.email_tx);
        mobile_tx = (TextInputEditText) findViewById(R.id.mobile_tx);
        phone_tx = (TextInputEditText) findViewById(R.id.phone_tx);


        male = (RadioButton) findViewById(R.id.male);
        female = (RadioButton) findViewById(R.id.female);


        databaseReference = FirebaseDatabase.getInstance().getReference().child("users").child(u_id);
        databaseReference.keepSynced(true);


    }

    @Override
    public void onStart() {
        super.onStart();

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String Address =  dataSnapshot.child("Address").getValue(String.class);
                String City =  dataSnapshot.child("City").getValue(String.class);
                String Email =  dataSnapshot.child("Email").getValue(String.class);
                String Gender =  dataSnapshot.child("Gender").getValue(String.class);
                String Mobile =  dataSnapshot.child("Mobile").getValue(String.class);
                String Phone =  dataSnapshot.child("Phone").getValue(String.class);

                city_tx.setText(City);
                address_tx.setText(Address);
                email_tx.setText(Email);
                mobile_tx.setText(Mobile);
                phone_tx.setText(Phone);

                if(Gender.equals("male"))
                    male.setChecked(true);
                else if(Gender.equals("female"))
                    female.setChecked(true);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    public void UpdateData(View view) {
       /* String Address =  dataSnapshot.child("Address").getValue(String.class);
        String City =  dataSnapshot.child("City").getValue(String.class);
        String Email =  dataSnapshot.child("Email").getValue(String.class);
        String Gender =  dataSnapshot.child("Gender").getValue(String.class);
        String Mobile =  dataSnapshot.child("Mobile").getValue(String.class);
        String Phone =  dataSnapshot.child("Phone").getValue(String.class);
*/

       // city_tx , address_tx , email_tx , mobile_tx  , phone_tx

        Map<String , Object > map = new HashMap<>();
        map.put("Address",address_tx.getText().toString());
        map.put("City",city_tx.getText().toString());
        map.put("Email",email_tx.getText().toString());
        map.put("Gender",male.isChecked()?"male":"female");
        map.put("Mobile",mobile_tx.getText().toString());
        map.put("Phone",phone_tx.getText().toString());

        databaseReference.updateChildren(map).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(UpdateExtraData.this, "Done update", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}
