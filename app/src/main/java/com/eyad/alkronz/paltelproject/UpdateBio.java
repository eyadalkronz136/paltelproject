package com.eyad.alkronz.paltelproject;

import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

public class UpdateBio extends AppCompatActivity {

    DatabaseReference databaseReference ;


    TextInputEditText textBioinput ;

    String userId ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_bio);

          userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        databaseReference = FirebaseDatabase.getInstance().getReference().child("users").child(userId);
        databaseReference.keepSynced(true);

        textBioinput = (TextInputEditText) findViewById(R.id.textBioinput);


        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String status =  dataSnapshot.child("BIO").getValue(String.class);
                  textBioinput.setText(status);
           }

            @Override
            public void onCancelled(DatabaseError databaseError) {  }
        });
    }


    public void DoupdateBio(View view) {
        String text = textBioinput.getText().toString();
        if(text.length()<5) {
            Toast.makeText(this, "Too short Question !!! ", Toast.LENGTH_SHORT).show();
            return;}
         databaseReference.child("BIO").setValue(text).addOnSuccessListener(new OnSuccessListener<Void>() {
             @Override
             public void onSuccess(Void aVoid) {
                 Toast.makeText(UpdateBio.this, "Update Bio  Done ", Toast.LENGTH_SHORT).show();
                 finish();
             }});
    }
}
