package com.eyad.alkronz.paltelproject.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.eyad.alkronz.paltelproject.R;
import com.eyad.alkronz.paltelproject.StartActivity;
import com.eyad.alkronz.paltelproject.UpdateExtraData;
import com.eyad.alkronz.paltelproject.UpdatePersonalData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import static com.eyad.alkronz.paltelproject.R.id.display_nametv;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {


    static final int REQUEST_VIDEO_CAPTURE = 1;



    TextView logout ,updatePersonalData  , uploadVideo ,uploadPDF , updatePersonalExtraData;
    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       View mView = inflater.inflate(R.layout.fragment_setting, container, false);




        logout = (TextView) mView.findViewById(R.id.logout);
        updatePersonalData = (TextView) mView.findViewById(R.id.updatePersonalData);
        uploadVideo = (TextView) mView.findViewById(R.id.uploadVideo);
        updatePersonalExtraData = (TextView) mView.findViewById(R.id.updatePersonalExtraData);
        uploadPDF = (TextView) mView.findViewById(R.id.uploadPDF);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(getContext() , StartActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });




        uploadPDF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("pdf/*");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        });

        updatePersonalExtraData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext() , UpdateExtraData.class);
                startActivity(intent);
            }
        });

        updatePersonalData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(getContext() , UpdatePersonalData.class);
                startActivity(intent);
            }
        });


        uploadVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    if (takeVideoIntent.resolveActivity(getContext().getPackageManager()) != null) {
                        startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
                    }


            }
        });

        return mView;
    }



}