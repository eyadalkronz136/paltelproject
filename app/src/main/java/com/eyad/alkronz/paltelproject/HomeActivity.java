package com.eyad.alkronz.paltelproject;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.eyad.alkronz.paltelproject.Adapters.MyOnNavigationItemSelectedListener;
import com.eyad.alkronz.paltelproject.Fragment.HomeFragment;
import com.eyad.alkronz.paltelproject.Fragment.NotificationFragment;
import com.eyad.alkronz.paltelproject.Fragment.ProfileFragment;
import com.eyad.alkronz.paltelproject.Utility.FragmentsUtil;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class HomeActivity extends AppCompatActivity {

    static final int REQUEST_VIDEO_CAPTURE = 1;

    StorageReference VideoReference;


    private FirebaseAuth mAuth;
    private Toolbar toolbar ;

    DatabaseReference databaseReference;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        VideoReference = FirebaseStorage.getInstance().getReference();

        mAuth = FirebaseAuth.getInstance();
       // mAuth.signOut();
        if(mAuth.getCurrentUser() == null)
            sendToStart();



        databaseReference = FirebaseDatabase.getInstance().getReference().child("users");

        // to initialization  BottomNavigationView
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(new MyOnNavigationItemSelectedListener(this));


        //to set Default Fragment
        Fragment fragment = new HomeFragment();
        FragmentsUtil.replaceFragment(this ,R.id.content ,fragment);

    }//end onCreate Method


    private void sendToStart() {
        Intent intent = new Intent(this , StartActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
       if(data.getData() ==null || data== null)
           return;

        Uri file  = data.getData();

        StorageReference riversRef = VideoReference.child("Videos/"+mAuth.getCurrentUser().getUid()+".mp4");
        UploadTask uploadTask = riversRef.putFile(file);
        Toast.makeText(this, "Upload video In Background  ....", Toast.LENGTH_SHORT).show();

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Toast.makeText(HomeActivity.this, "Error in Upload Your Video  ", Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                 Uri downloadUrl = taskSnapshot.getDownloadUrl();
                Toast.makeText(HomeActivity.this, "Done upload Video", Toast.LENGTH_SHORT).show();
                databaseReference .child(mAuth.getCurrentUser().getUid()).child("VideoURL").setValue(downloadUrl.toString());
            }});
    }//end on Avtivity Result





}
