package com.eyad.alkronz.paltelproject.NotificationClient;

import com.eyad.alkronz.paltelproject.Entity.MyNotification;

import org.json.simple.JSONObject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Eyad on 8/18/2017.
 */

public class SendData{

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    final String url = "https://fcm.googleapis.com/fcm/send";
    OkHttpClient httpClient = HttpClient.getInsance();


    String Server_key =
                    "AAAAE9gvWjg:APA91bH7tuTRc65PskGynqstSyBWiK-EWOyLdzrh6sT9ZYLIB-slRFx4Xa91Jg3BtAhJhGrqe3sxPOX7gKyZzzlrZXas3KLl81XJx8_eUIMYWv4HEZSBoepVZS_vl4-9TrebuZ8TyDKD";
    String token ;
    MyNotification notification;

     public SendData(String token , MyNotification notification) {
         this.token = token;
         this.notification = notification;
     }





    public String doSend() {
         JSONObject root = new JSONObject();
        root.put("to", token);
        root.put("priority", "high");

        JSONObject data = new JSONObject();


        /*
            public String   key ;
    public String   sender_id ;
    public String   message ;
    public String   deleted ;
    public String   status ;
    public String   timestamp ;
    public String   type ;
    public String   reciver_id ;
         */
        data.put("key", notification.key);
        data.put("message", notification.message);
        data.put("deleted", notification.deleted);
        data.put("status", notification.status);
        data.put("timestamp", notification.timestamp);
        data.put("type", notification.type);
        data.put("userId", notification.userId);


        root.put("data", data);

        RequestBody body = RequestBody.create(JSON, root.toString());
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader("Authorization", "key="+Server_key)
                .build();
        Response response ;
        try {
            response = httpClient.newCall(request).execute();
            System.out.println("In Do send Done");
             return response.body().string();
         }catch (Exception ex) {
            System.out.println("In Do send Exception "+ ex );
            return ex.getMessage();
         }



    }


}