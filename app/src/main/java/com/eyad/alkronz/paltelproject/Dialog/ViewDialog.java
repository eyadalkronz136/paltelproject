package com.eyad.alkronz.paltelproject.Dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Toast;

import com.eyad.alkronz.paltelproject.QuestionComment;
import com.eyad.alkronz.paltelproject.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class ViewDialog {

    DatabaseReference CommentDatabaseReference;

   static String Question_Id;

    public void showDialog(final Activity activity, String Question_Idd){
      Question_Id=Question_Idd;
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog);

        CommentDatabaseReference = FirebaseDatabase.getInstance().getReference().child("Comments");
        CommentDatabaseReference.keepSynced(true);


        final TextInputEditText text = (TextInputEditText) dialog.findViewById(R.id.text_dialog);



        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

               /* String Qid ;
                String Comment_id;
                String User_id;
                long timeStamp ;
                    public String content ;

                */
                String commentId = CommentDatabaseReference.push().getKey();
                Map<String ,Object> map = new HashMap<String, Object>();
                map.put("Qid" ,Question_Id );
                map.put("Comment_id",commentId);
                String u_id = FirebaseAuth.getInstance().getCurrentUser().getUid();
                map.put("User_id",u_id);
                map.put("content",text.getText().toString());
                map.put("timeStamp",new Date().getTime());
                map.put("timeStamp",new Date().getTime());

                CommentDatabaseReference.child(Question_Id).child(commentId).setValue(map).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Intent intent = new Intent(activity, QuestionComment.class);
                        intent.putExtra("Question_id",Question_Id);
                        activity.startActivity(intent);
                     }
                });




             }
        });

        dialog.show();

    }
}