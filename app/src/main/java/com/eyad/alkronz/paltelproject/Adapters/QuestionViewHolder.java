package com.eyad.alkronz.paltelproject.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.eyad.alkronz.paltelproject.Dialog.ViewDialog;
import com.eyad.alkronz.paltelproject.Entity.Question;
import com.eyad.alkronz.paltelproject.QuestionComment;
import com.eyad.alkronz.paltelproject.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Eyad on 9/1/2017.
 */

public   class QuestionViewHolder extends RecyclerView.ViewHolder {
    DatabaseReference QueDatabaseReference;
    DatabaseReference UsersDatabaseReference;

    static String u_id;




    View mView;
    CircleImageView question_image;
    TextView question_name, question_date, question_txt;
    Button like_question, comment_question;


    public QuestionViewHolder(View itemView) {
        super(itemView);
        mView = itemView;

        QueDatabaseReference = FirebaseDatabase.getInstance().getReference().child("Questions");
        UsersDatabaseReference = FirebaseDatabase.getInstance().getReference().child("users");
        QueDatabaseReference.keepSynced(true);
        UsersDatabaseReference.keepSynced(true);

        question_image = (CircleImageView) itemView.findViewById(R.id.comment_image);
        question_name = (TextView) itemView.findViewById(R.id.question_name);
        question_date = (TextView) itemView.findViewById(R.id.question_date);
        question_txt = (TextView) itemView.findViewById(R.id.question_txt);

        like_question = (Button) itemView.findViewById(R.id.like_question);
        comment_question = (Button) itemView.findViewById(R.id.comment_question);


    }

    public void setDate(final Question model, boolean isSelected) {

        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mView.getContext(), QuestionComment.class);
                intent.putExtra("Question_id", model.Q_Id);
                mView.getContext().startActivity(intent);
            }
        });


        comment_question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new ViewDialog().showDialog((Activity) mView.getContext(), model.Q_Id);
            }
        });


        if (isSelected) {
            Drawable img = ContextCompat.getDrawable(mView.getContext(), R.drawable.ic_like);
            img.setBounds(0, 0, 60, 60);
            like_question.setCompoundDrawables(img, null, null, null);

            like_question.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    //  QueDatabaseReference.child(model.Q_Id).child("likes").child(u_id).setValue(u_id);
                    QueDatabaseReference.child(model.Q_Id).child("likes").child(u_id).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            //just change image
                            Drawable img = ContextCompat.getDrawable(mView.getContext(), R.drawable.ic_dis_like);
                            img.setBounds(0, 0, 60, 60);
                            like_question.setCompoundDrawables(img, null, null, null);


                        }
                    });

                }
            });


        } else {
            Drawable img = ContextCompat.getDrawable(mView.getContext(), R.drawable.ic_dis_like);
            img.setBounds(0, 0, 60, 60);
            like_question.setCompoundDrawables(img, null, null, null);


            like_question.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(mView.getContext(), "Send Notification ?!? ", Toast.LENGTH_SHORT).show();


                    QueDatabaseReference.child(model.Q_Id).child("likes").child(u_id).setValue(u_id).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            //just change image
                            Drawable img = ContextCompat.getDrawable(mView.getContext(), R.drawable.ic_like);
                            img.setBounds(0, 0, 60, 60);
                            like_question.setCompoundDrawables(img, null, null, null);

                            //code to send notification from current user to this user !!


                        }
                    });

                }
            });

        }


        UsersDatabaseReference.child(model.UserId).child("FullName").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) return;
                question_name.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });


        UsersDatabaseReference.child(model.UserId).child("ThumbImage").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) return;
                ;

                try {


                    Picasso.with(mView.getContext()).load(dataSnapshot.getValue().toString()).networkPolicy(NetworkPolicy.OFFLINE).
                            into(question_image, new Callback() {
                                @Override
                                public void onSuccess() {
                                }

                                @Override
                                public void onError() {
                                    Picasso.with(mView.getContext()).load(dataSnapshot.getValue().toString()).into(question_image);
                                }
                            });
                } catch (Exception ex) {
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });


        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String reportDate = df.format(model.Timestamp);

        question_date.setText(reportDate + "");
        question_txt.setText(model.Content);


    }
}
