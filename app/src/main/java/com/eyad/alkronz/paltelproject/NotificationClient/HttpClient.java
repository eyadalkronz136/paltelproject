package com.eyad.alkronz.paltelproject.NotificationClient;

import okhttp3.OkHttpClient;

/**
 * Created by Eyad on 8/18/2017.
 */

public class HttpClient {

    static OkHttpClient instance ;
    private HttpClient(){}


    public static OkHttpClient getInsance(){
        if(instance ==null) return new OkHttpClient();
        return instance;
    }
}
