package com.eyad.alkronz.paltelproject.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import com.eyad.alkronz.paltelproject.Fragment.EventsFragment;
import com.eyad.alkronz.paltelproject.Fragment.HomeFragment;
import com.eyad.alkronz.paltelproject.Fragment.NotificationFragment;
import com.eyad.alkronz.paltelproject.Fragment.ProfileFragment;
import com.eyad.alkronz.paltelproject.Fragment.SettingsFragment;

/**
 * Created by Eyad on 8/3/2017.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter {
    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0 : return  new HomeFragment();
            case 1 : return  new ProfileFragment();
            case 2 : return  new NotificationFragment();
            case 3 : return  new EventsFragment();
            case 4 : return  new SettingsFragment();
            default: return  null;
        }
    }

    @Override
    public int getCount() {
        return 5;
    }

    /*
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0: return "Home";
            case 1: return "Profile";
            case 2: return "Notifications";
            default: return  null;
        }

    }*/
}
