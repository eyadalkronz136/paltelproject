package com.eyad.alkronz.paltelproject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

public class LoginActivity extends AppCompatActivity {

    Button btn_login ;
    final String TAG  = "FirebaseAuth :";
    private FirebaseAuth mAuth;
    private TextInputEditText email_tx , password_tx    ;

    ProgressDialog mProgressDialog ;
    private DatabaseReference mDatabaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_login);

        btn_login = (Button) findViewById(R.id.btn_login);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginclick();
            }
        });

        mProgressDialog = new ProgressDialog(this);

         mAuth = FirebaseAuth.getInstance();

        email_tx = (TextInputEditText) findViewById(R.id.email_tx_login);
        password_tx = (TextInputEditText) findViewById(R.id.password_tx_login);



        //update it later
       // mDatabaseReference = FirebaseDatabase.getInstance().getReference().child("users");

    }



    public void loginclick(   ) {
        String email = email_tx.getText().toString();
        String password = password_tx.getText().toString();


        if (!(TextUtils.isEmpty(email) || TextUtils.isEmpty(password))) {
            mProgressDialog.setTitle("log in");
            mProgressDialog.setMessage("please wait while varify your data ... ");
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.show();

            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                mProgressDialog.dismiss();
                                Log.d(TAG, "signInWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();

                                mProgressDialog.dismiss();
                               // FirebaseUser currentuser = FirebaseAuth.getInstance().getCurrentUser();
                               // final String device_token = FirebaseInstanceId.getInstance().getToken();


                                Toast.makeText(LoginActivity.this, "Authentication success.",
                                        Toast.LENGTH_SHORT).show();
                               Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_CLEAR_TASK);
                               startActivity(intent);


                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w(TAG, "signInWithEmail:failure", task.getException());
                                Toast.makeText(LoginActivity.this, "Authentication failed." + task.getException(),
                                        Toast.LENGTH_SHORT).show();
                                mProgressDialog.hide();                            }
                          }
                    });
          }//end if all data is true
    }//end method
}//end class
