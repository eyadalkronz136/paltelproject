package com.eyad.alkronz.paltelproject.Entity;

/**
 * Created by Eyad on 8/25/2017.
 */

public class User {

    public  String Address;
    public  String City;
    public  long DOB;
    public  String UserId;
    public  String DeviceToken;
    public  String Email;
    public  String Gender;
    public  String Mobile;
    public  String FullName;
    public  String Phone;
    public  String Platform;
    public  long RegistrationDate;
    public  String ThumbImage;
    public  String UserImage;
    public  String BIO;
    public  String Type;
    public  String VideoThumbURL;
    public  String VideoURL;
    public  long Longitude;
    public  long Latitude;


    public User(   ) {}


    public String getBIO() {
        return BIO;
    }

    public void setBIO(String BIO) {
        this.BIO = BIO;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getAddress() {
        return Address;
    }

    public String getCity() {
        return City;
    }

    public long getDOB() {
        return DOB;
    }

    public String getDeviceToken() {
        return DeviceToken;
    }

    public String getEmail() {
        return Email;
    }

    public String getGender() {
        return Gender;
    }

    public String getMobile() {
        return Mobile;
    }

    public String getPhone() {
        return Phone;
    }

    public String getPlatform() {
        return Platform;
    }

    public long getRegistrationDate() {
        return RegistrationDate;
    }

    public String getThumbImage() {
        return ThumbImage;
    }

    public String getUserImage() {
        return UserImage;
    }

    public String getType() {
        return Type;
    }

    public String getVideoThumbURL() {
        return VideoThumbURL;
    }

    public String getVideoURL() {
        return VideoURL;
    }



    public void setAddress(String address) {
        Address = address;
    }

    public void setCity(String city) {
        City = city;
    }

    public void setDOB(long DOB) {
        this.DOB = DOB;
    }

    public void setDeviceToken(String deviceToken) {
        DeviceToken = deviceToken;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public void setPlatform(String platform) {
        Platform = platform;
    }

    public void setRegistrationDate(long registrationDate) {
        RegistrationDate = registrationDate;
    }

    public void setThumbImage(String thumbImage) {
        ThumbImage = thumbImage;
    }

    public void setUserImage(String userImage) {
        UserImage = userImage;
    }

    public void setType(String type) {
        Type = type;
    }

    public void setVideoThumbURL(String videoThumbURL) {
        VideoThumbURL = videoThumbURL;
    }

    public void setVideoURL(String videoURL) {
        VideoURL = videoURL;
    }

    public long getLongitude() {
        return Longitude;
    }

    public long getLatitude() {
        return Latitude;
    }

    public void setLongitude(long longitude) {
        Longitude = longitude;
    }

    public void setLatitude(long latitude) {
        Latitude = latitude;
    }
}
