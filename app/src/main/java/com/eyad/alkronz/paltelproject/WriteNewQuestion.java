package com.eyad.alkronz.paltelproject;

import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.eyad.alkronz.paltelproject.Entity.Question;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;

public class WriteNewQuestion extends AppCompatActivity {
    DatabaseReference QueDatabaseReference;

    TextInputEditText textinput ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_new_question);

        QueDatabaseReference =  FirebaseDatabase.getInstance().getReference().child("Questions");


        textinput = (TextInputEditText) findViewById(R.id.textinput);
    }

    public void DoSendQuestion(View view) {
        String text = textinput.getText().toString();
        if(text.length()<5) {
            Toast.makeText(this, "Too short Question !!! ", Toast.LENGTH_SHORT).show();
            return;}
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
         Question question   = new Question();
        question. Content= text;
        question. Timestamp= new Date().getTime();
        question. UserId= userId;

        String key = QueDatabaseReference.push().getKey();
        question.Q_Id=key;
        QueDatabaseReference.child(key).setValue(question).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                 finish();
            }
        });
    }
}
