package com.eyad.alkronz.paltelproject.Fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.eyad.alkronz.paltelproject.Adapters.QuestionViewHolder;
import com.eyad.alkronz.paltelproject.Dialog.ViewDialog;
import com.eyad.alkronz.paltelproject.Entity.Question;
import com.eyad.alkronz.paltelproject.FullscreenActivityToShowVideo;
import com.eyad.alkronz.paltelproject.QuestionComment;
import com.eyad.alkronz.paltelproject.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;




public class ProfileFragment extends Fragment {


    static Context FragmentContext;
      DatabaseReference QueDatabaseReference;
      DatabaseReference UsersDatabaseReference;
    View mView;

    static String u_id;
    CircleImageView user_image;

    TextView user_name, user_status;
    RecyclerView questionList;
    Button ShowVideo_btn;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_profile, container, false);


        ShowVideo_btn = (Button) mView.findViewById(R.id.ShowVideo_btn);
        ShowVideo_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             Intent intent = new Intent(getContext() , FullscreenActivityToShowVideo.class);
                startActivity(intent );
            }});

        user_name = (TextView) mView.findViewById(R.id.user_name);
        user_status = (TextView) mView.findViewById(R.id.user_status);
        user_image = (CircleImageView) mView.findViewById(R.id.user_image_profile);


        u_id = FirebaseAuth.getInstance().getCurrentUser().getUid();


        QueDatabaseReference = FirebaseDatabase.getInstance().getReference().child("Questions");
        UsersDatabaseReference = FirebaseDatabase.getInstance().getReference().child("users");
        QueDatabaseReference.keepSynced(true);
        UsersDatabaseReference.keepSynced(true);


        questionList = (RecyclerView) mView.findViewById(R.id.questionList);
        questionList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));



        UsersDatabaseReference.child(u_id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) return;
                String user_image_path = dataSnapshot.child("UserImage").getValue().toString();
                if (user_image_path.equals("default")) {

                } else {
                     Picasso.with(getContext()).load(user_image_path).placeholder(ContextCompat.getDrawable(getContext(), R.drawable.default_profle_image_male)).networkPolicy(NetworkPolicy.OFFLINE).
                            into(user_image, new Callback() {
                                @Override
                                public void onSuccess() {
                                 }

                                @Override
                                public void onError() {
                                    Picasso.with(getContext()).load(dataSnapshot.getValue().toString()).into(user_image);

                                }
                            });

                }
                user_name.setText(dataSnapshot.child("FullName").getValue().toString());
                user_status.setText(dataSnapshot.child("BIO").getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });


        final List<Question> questions = new ArrayList<>();

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("Questions");


        Query query = reference.orderByChild("UserId").equalTo(u_id);
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                questions.add(dataSnapshot.getValue(Question.class));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


//

        RecyclerView.Adapter<QuestionViewHolder> myAdapter = new RecyclerView.Adapter<QuestionViewHolder>() {
            @Override
            public QuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View mView = LayoutInflater.from(getContext()).inflate(R.layout.question_row, parent, false);
                return new QuestionViewHolder(mView);
            }

            @Override
            public void onBindViewHolder(final QuestionViewHolder holder, final int position) {

                QueDatabaseReference.child(questions.get(position).Q_Id).child("likes").child(u_id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists())
                            holder.setDate(questions.get(position), true);
                        else
                            holder.setDate(questions.get(position), false);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

            }//eend on Binding


            @Override
            public int getItemCount() {
                return questions.size();
            }
        };


        questionList.setAdapter(myAdapter);

        return mView;

    }//end on start



}
