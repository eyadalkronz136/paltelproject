package com.eyad.alkronz.paltelproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
    }


    public void goToRegisterActivity(View view) {
        Intent intent = new Intent(this , RegisterActivity.class);
        startActivity(intent);
      //  finish();
    }

    public void goToSignInActivity(View view) {
        Intent intent = new Intent(this ,LoginActivity.class);
        startActivity(intent);
    }
}
