package com.eyad.alkronz.paltelproject.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.eyad.alkronz.paltelproject.Adapters.QuestionViewHolder;
import com.eyad.alkronz.paltelproject.Entity.Question;
import com.eyad.alkronz.paltelproject.R;
import com.eyad.alkronz.paltelproject.WriteNewQuestion;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeFragment extends Fragment {
    static String u_id;

    AppBarLayout app_bar;
    RecyclerView feeds;
    static DatabaseReference UsersDatabaseReference;
    DatabaseReference QueDatabaseReference;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_home, container, false);

        u_id = FirebaseAuth.getInstance().getCurrentUser().getUid();

        QueDatabaseReference = FirebaseDatabase.getInstance().getReference().child("Questions");
        QueDatabaseReference.keepSynced(true);
        UsersDatabaseReference = FirebaseDatabase.getInstance().getReference().child("users");
        UsersDatabaseReference.keepSynced(true);






        feeds = (RecyclerView) mView.findViewById(R.id.feeds);
        feeds.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, true));
        return mView;
    }


    @Override
    public void onStart() {
        super.onStart();


        FirebaseRecyclerAdapter<Question, QuestionViewHolder> firebaseRecyclerAdapter =
                new FirebaseRecyclerAdapter<Question, QuestionViewHolder>(
                        Question.class,
                        R.layout.question_row,
                          QuestionViewHolder.class,
                        QueDatabaseReference
                ) {
                    @Override
                    protected void populateViewHolder(final  QuestionViewHolder viewHolder, final Question model, int position) {
                        QueDatabaseReference.child(model.Q_Id).child("likes").child(u_id).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (viewHolder!=null && dataSnapshot.exists()){
                                   viewHolder.setDate(model, true);
                                     }
                                else
                                     viewHolder.setDate(model, false);


                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) { }});
                    }
                };
        feeds.setAdapter(firebaseRecyclerAdapter);
    }

}