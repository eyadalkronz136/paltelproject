package com.eyad.alkronz.paltelproject;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.eyad.alkronz.paltelproject.Dialog.ViewDialog;
import com.eyad.alkronz.paltelproject.Entity.Comment;
import com.eyad.alkronz.paltelproject.Entity.Question;
import com.eyad.alkronz.paltelproject.Fragment.ProfileFragment;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import de.hdodenhof.circleimageview.CircleImageView;

public class QuestionComment extends AppCompatActivity {

     RecyclerView comments_list;

    DatabaseReference CommentDatabaseReference;
    DatabaseReference QueDatabaseReference;
    static DatabaseReference UsersDatabaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_comment);

        String Question_id = getIntent().getStringExtra("Question_id");


        comments_list= (RecyclerView) findViewById(R.id.comments_list);
        comments_list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));



        QueDatabaseReference = FirebaseDatabase.getInstance().getReference().child("Questions");
        QueDatabaseReference.keepSynced(true);
        UsersDatabaseReference = FirebaseDatabase.getInstance().getReference().child("users");
        UsersDatabaseReference.keepSynced(true);
        CommentDatabaseReference = FirebaseDatabase.getInstance().getReference().child("Comments").child(Question_id);
        CommentDatabaseReference.keepSynced(true);



    }




    @Override
    public void onStart() {
        super.onStart();


        FirebaseRecyclerAdapter<Comment, CommentViewHolder> firebaseRecyclerAdapter =
                new FirebaseRecyclerAdapter<Comment, CommentViewHolder>(
                        Comment.class,
                        R.layout.comment_row,
                        CommentViewHolder.class,
                        CommentDatabaseReference
                ){
                    @Override
                    protected void populateViewHolder(final CommentViewHolder viewHolder, final Comment model, int position) {
                         viewHolder.setDate(model);
                    }
                             @Override
                            public void onCancelled(DatabaseError databaseError) { }
        };
         comments_list.setAdapter(firebaseRecyclerAdapter);
    }



public  static class CommentViewHolder extends RecyclerView.ViewHolder{
    View mView;
    CircleImageView comment_image;
    TextView comment_name , comment_date , comment_txt ;


    public CommentViewHolder(View itemView) {
        super(itemView);
        mView =itemView;

        comment_image = (CircleImageView) itemView.findViewById(R.id.comment_image);
        comment_date = (TextView) itemView.findViewById(R.id.comment_date);
        comment_name = (TextView) itemView.findViewById(R.id.comment_name);
        comment_txt = (TextView) itemView.findViewById(R.id.comment_txt);
     }

    public void setDate(final Comment model  ) {
         UsersDatabaseReference.child( model.User_id).child("FullName").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()==null ) return;
                comment_name.setText(dataSnapshot.getValue().toString());
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}});



        UsersDatabaseReference.child( model.User_id).child("ThumbImage").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() ==null) return;;

                Picasso.with(mView.getContext()).load(  dataSnapshot.getValue().toString()).networkPolicy(NetworkPolicy.OFFLINE).
                        into(comment_image, new Callback() {
                            @Override
                            public void onSuccess() {    }
                            @Override
                            public void onError() {
                                Picasso.with(mView.getContext()).load(dataSnapshot.getValue().toString()).into(comment_image);
                            }
                        });
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}});


        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String reportDate = df.format(model.timeStamp);

        comment_date.setText(reportDate+"");
        comment_txt.setText(model.content);


    }
}


}
