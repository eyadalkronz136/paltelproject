package com.eyad.alkronz.paltelproject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {
    private Toolbar toolbar ;

    RadioButton male  ;
    final String TAG  = "FirebaseAuth :";
    private FirebaseAuth mAuth;
    private TextInputEditText email_tx , password_tx , displayname;

    ProgressDialog mProgressDialog ;


    DatabaseReference databaseReference ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        toolbar = (Toolbar) findViewById(R.id.main_app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Create Account");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        male = (RadioButton) findViewById(R.id.male);

        mProgressDialog = new ProgressDialog(this);

        mAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("users");


        email_tx = (TextInputEditText) findViewById(R.id.email_tx);
        password_tx = (TextInputEditText) findViewById(R.id.password_tx);
        displayname = (TextInputEditText) findViewById(R.id.displayname);
    }


    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        //updateUI(currentUser);
    }


    public void signupClick(View view) {

        final String email = email_tx.getText().toString();
        final String password = password_tx.getText().toString();
         final String name = displayname.getText().toString();
        final String gender = (male.isSelected() ? "male": "female");

        if (!(TextUtils.isEmpty(email) || TextUtils.isEmpty(password))) {

            mProgressDialog.setTitle("Register User");
            mProgressDialog.setMessage("please wait while we create your account ... ");
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.show();

            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "createUserWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                Toast.makeText(RegisterActivity.this, "Authentication success.",Toast.LENGTH_SHORT).show();

                                FirebaseUser currentuser = FirebaseAuth.getInstance().getCurrentUser();
                                String uid = currentuser.getUid();
                                String device_token = FirebaseInstanceId.getInstance().getToken();

                                databaseReference= databaseReference.child(uid);
                                HashMap<String,Object> user_map = new HashMap<String, Object>();
                                user_map.put("FullName",name);
                                user_map.put("Email",email);
                                user_map.put("Gender",gender);
                                user_map.put("DeviceToken",device_token);
                                user_map.put("BIO","Default  status");
                                user_map.put("ThumbImage","default");
                                user_map.put("UserImage","default");
                                user_map.put("Address","default");
                                user_map.put("City","default");
                                user_map.put("DOB","default");
                                user_map.put("UserId",uid);
                                  user_map.put("Mobile","default");
                                user_map.put("Phone","default");
                                user_map.put("Platform","android");
                                user_map.put("RegistrationDate","android");
                                user_map.put("Type","default");
                                user_map.put("VideoThumbURL","default");
                                user_map.put("VideoURL","default");
                                user_map.put("Latitude",0);
                                user_map.put("Longitude",0);


                                 /*User tempuser = new User();
                                tempuser.setFullName(name);
                                tempuser.setEmail(email);
                                tempuser.setGender(gender);
                                tempuser.setDeviceToken(device_token);
                                tempuser.setUserId(device_token);
                                tempuser.setUserImage("default");
                                tempuser.setThumbImage("default");
                                */

                                databaseReference.setValue(user_map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){

                                            mProgressDialog.dismiss();
                                            Intent intent = new Intent(RegisterActivity.this, UpdatePersonalData.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                             startActivity(intent);
                                             finish();
                                        }
                                    }
                                });
                             } else {
                                // If sign in fails, display a message to the user.
                                Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                Toast.makeText(RegisterActivity.this, "Authentication failed." + task.getException(),
                                        Toast.LENGTH_SHORT).show();
                                mProgressDialog.hide();
                            }
                            // ...
                        }
                    });

        }//if all input is true


    }



}
