package com.eyad.alkronz.paltelproject.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eyad.alkronz.paltelproject.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventsFragment extends Fragment {
    RecyclerView listOfFriends;
    DatabaseReference friDatabaseReference, userDatabaseReference;
    FirebaseAuth mAuth;
    String mCurrentUser_id;
    View mView;


    public EventsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_events, container, false);


        return mView;
    }
}

